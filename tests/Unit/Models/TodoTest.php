<?php

namespace Tests\Unit\Models;

use App\Models\Todo;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class TodoTest extends TestCase
{
    use WithFaker;
    use DatabaseTransactions;

    public function testTodoListPropertiesIsGetCorrectValues()
    {
        $list = Todo::$list;

        $this->assertCount(3, $list);

        $this->assertTrue(in_array("open", \array_keys($list)));
        $this->assertTrue(in_array("doing", \array_keys($list)));
        $this->assertTrue(in_array("closed", \array_keys($list)));

        $this->assertSame(1, Todo::OPEN);
        $this->assertSame(2, Todo::DOING);
        $this->assertSame(3, Todo::CLOSED);
    }

    public function testTodoCreateTodoMethodCanCreateDatabaseTodoRow()
    {
        $fakeTodo = Todo::factory()->create();

        $todoCount = Todo::count();

        $todo = Todo::createTodo(
            $fakeTodo->title,
            $fakeTodo->description,
            $fakeTodo->status,
            $fakeTodo->expired_at,
        );

        $this->assertSame($todoCount + 1, Todo::count());

        $this->assertSame($todo->title, $fakeTodo->title);
        $this->assertSame($todo->description, $fakeTodo->description);
        $this->assertSame($todo->status, $fakeTodo->status);
        $this->assertSame($todo->expired_at, $fakeTodo->expired_at);
    }
}
