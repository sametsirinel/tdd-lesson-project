<?php

namespace Database\Factories;

use App\Models\Todo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Todo>
 */
class TodoFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            "title" => $this->faker->name(),
            "description" => $this->faker->realText(),
            "expired_at" => Carbon::now()->addDays(rand(1, 7)),
            "status" => $this->faker->randomElement(Todo::$list),
        ];
    }
}
