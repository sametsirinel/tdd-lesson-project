<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Todo extends Model
{
    use HasFactory;

    public const OPEN = 1;
    public const DOING = 2;
    public const CLOSED = 3;

    public static array $list = [
        "open" => self::OPEN,
        "doing" => self::DOING,
        "closed" => self::CLOSED,
    ];

    protected $fillable = [
        "title",
        "description",
        "expired_at",
        "status"
    ];

    public function scopeCreateTodo(
        $query,
        string $title,
        string $description = null,
        int $status = null,
        Carbon $expired_at = null
    ): self {
        return $query->create([
            "title" => $title,
            "description" => $description,
            "expired_at" => $expired_at,
            "status" => $status
        ]);
    }
}
